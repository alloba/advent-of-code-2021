# and bingo was his name-o

input_raw = open('input-files/day4-input.txt').read()
working_draws = input_raw.split('\n')[0].split(',')

input_boards = input_raw.split('\n\n')[1:]
working_boards = []
for board_string in input_boards:
    board_final = []
    rows = board_string.split('\n')
    for row in rows:
        vals = row.split()
        board_final.extend(vals)
    working_boards.append(board_final)

score_boards = []
for index, board in enumerate(working_boards):
    score_boards.append([])
    for entry in board:
        score_boards[index].append(0)


def generate_scoreboard(board_in):
    return [0 for x in range(len(board_in))]


def get_row(board_entry, width, index_val):
    res = []
    for i in range(width):
        res.append(board_entry[index_val * width + i])
    return res


def get_col(board_entry, width, index_val):
    res = []
    for i in range(width):
        res.append(board_entry[index_val + i * width])
    return res


def get_first_winning_board(draws, boards):
    score_boardz = [generate_scoreboard(x) for x in boards]
    called_draws = []
    for draw in draws:
        called_draws.append(draw)
        for index, board in enumerate(boards):
            for zindex, val in enumerate(board):
                if val == draw:
                    score_boardz[index][zindex] = 1
            for i in range(5):
                row = get_row(score_boardz[index], 5, i)
                col = get_col(score_boardz[index], 5, i)

                if row.count(1) == 5 or col.count(1) == 5:
                    return boards[index], score_boardz[index], draw, index


def calculate_board_score(board_in, scoreboard, final_draw):
    scores = []
    for z in range(len(board_in)):
        if scoreboard[z] == 1:
            scores.append(0)
        else:
            scores.append(int(board_in[z]))

    final_sum = sum([int(x) for x in scores])
    return final_sum * int(final_draw)


def part1():
    win_board, win_scoreboard, win_draw, win_index = get_first_winning_board(working_draws, working_boards)
    return calculate_board_score(win_board, win_scoreboard, win_draw)


def part2():
    while len(working_boards) > 1:
        win_board, win_scoreboard, win_draw, win_index = get_first_winning_board(working_draws, working_boards)
        del working_boards[win_index]

    win_board, win_scoreboard, win_draw, win_index = get_first_winning_board(working_draws, working_boards)
    return calculate_board_score(win_board, win_scoreboard, win_draw)


print(part1())
print(part2())

