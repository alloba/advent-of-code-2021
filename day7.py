import math
import statistics


def load_input():
    crabs = [int(x) for x in open('input-files/day7-input.txt').read().split(',')]
    return crabs


def part1():
    # Cheapest target is the median of all starting positions.
    # Then the cost is just the distance from that value, for all crabs.
    crabs = load_input()
    target = statistics.median(crabs)
    print('Target:', target)
    return sum([abs(target - c) for c in crabs])


def part2():
    # Based off a guess, increasingly expensive movement changes the target position to the mean, instead of the median.
    crabs = load_input()
    target1 = math.ceil(statistics.mean(crabs))   # i have no patience to figure out which target is correct in which case,
    target2 = math.floor(statistics.mean(crabs))  # all i know is that in this particular instance, the floor was correct and the ceiling was not.

    total_expense = 0
    for c in crabs:
        cost_of_movement = 0
        single_crab_expense = 0
        for i in range(abs(target2 - c)):
            cost_of_movement += 1
            single_crab_expense += cost_of_movement
        total_expense += single_crab_expense

    return total_expense


part1_solution = part1()
part2_solution = part2()

print(f'Part 1 solution: {part1_solution}')
print(f'Part 2 solution: {part2_solution}')
