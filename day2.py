#!/usr/bin/env python

def part1():
    input_value = open('input-files/day2-input.txt').readlines()
    horizontal = 0
    vertical = 0

    for command in input_value:
        order, magnitude = command.split(' ')
        magnitude = int(magnitude)
        if order == 'forward':
            horizontal += magnitude
        elif order == 'up':
            vertical -= magnitude
        elif order == 'down':
            vertical += magnitude
    return horizontal * vertical


def part2():
    input_value = open('input-files/day2-input.txt').readlines()
    horizontal = 0
    vertical = 0
    aim = 0

    for command in input_value:
        order, magnitude = command.split(' ')
        magnitude = int(magnitude)
        if order == 'forward':
            horizontal += magnitude
            vertical += (aim * magnitude)
        elif order == 'up':
            aim -= magnitude
        elif order == 'down':
            aim += magnitude
    return horizontal * vertical


print(f'Part 1 Answer: {part1()}')
print(f'Part 2 Answer: {part2()}')
