def load_input():
    fish = [int(x) for x in open('input-files/day6-input.txt').read().split(',')]
    return fish


def tick_fish(fish_list):
    fish_list_new = [x - 1 for x in fish_list]
    new_fish = []

    for i in range(len(fish_list_new)):
        if fish_list_new[i] == -1:
            new_fish.append(8)
            fish_list_new[i] = 6

    fish_list_new.extend(new_fish)
    return fish_list_new


def gather_fish(initial_fish, days):
    new_fish = [x for x in initial_fish]
    for i in range(days):
        # print('Day {}'.format(i))
        new_fish = tick_fish(new_fish)
    return new_fish


def part1():
    return len(gather_fish(load_input(), 80))


def part2():
    # if a fish at age x generates k fish, you can apply that value to all similarly aged fish...
    # max age is 8, which is small enough to manually handle.
    # so get values for all ages, for 128 days. then apply?

    fish_samples = {}
    for i in range(9):
        print("Gathering sample for age {}".format(i))
        entry = {
            'count': len(gather_fish([i], 128)),
            'list': gather_fish([i], 128)
        }
        fish_samples[i] = entry

    # now we have all the samples, we can apply them to the fish
    print('Half Fishing')
    half_fish = []
    for fish in load_input():
        new_fish = fish_samples[fish]['list']
        half_fish.extend(new_fish)

    # for the final stretch, we're fighting in-memory list modification of huge numbers.
    # we don't need the actual fish to jump to another stage, so ditch them and just use the counts.
    print('Whole Fishing:')
    whole_fish_count = 0
    for index, fish in enumerate(half_fish):
        if index % 1000 == 0:
            print('{}/{}'.format(index, len(half_fish)))
        whole_fish_count += fish_samples[fish]['count']
    return whole_fish_count


part1_solution = part1()
part2_solution = part2()

print(f'The solution to part 1 is: {part1_solution}')
print(f'The solution to part 2 is: {part2_solution}')

