import sys

def part1():
    in_val = open('input-files/day1-input.txt').readlines()
    prev = sys.maxsize
    times_increased=0

    for val in in_val:
        if int(val) > prev:
            times_increased += 1
        prev = int(val)
    return times_increased

def part2():
    in_val = [int(x) for x in open('input-files/day1-input.txt').readlines()]
    times_increased=0
    prev_sum = sys.maxsize
    for i in range(3, len(in_val)):
        curr_sum = in_val [i] + in_val[i-1] + in_val[i-2]
        if curr_sum > prev_sum:
            times_increased += 1
        prev_sum = curr_sum
    return times_increased

print(part1())
print(part2())
