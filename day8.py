def load_input():
    lines = open('input-files/day8-input.txt').readlines()
    separated = [[line.split('|')[0].strip(), line.split('|')[1].strip()] for line in lines]
    return separated


def find_segment_count(line, count):
    """
    count | actual_number
    2     | 1
    4     | 4
    3     | 7
    7     | 8
    """
    if count not in [2, 4, 3, 7]:
        raise Exception('Valid input can only be segment counts that are guaranteed unique')

    found_count = 0
    segment_list = ''
    for item in line[1].split(' '):
        if len(item) == count:
            found_count += 1
            segment_list = item
    return found_count, segment_list


def get_segment_matcher(line, count):
    candidates = list(filter(lambda val: len(val) == count, line[0].split(' ')))
    candidates = [''.join(sorted(x)) for x in candidates]
    return candidates


def part1():
    values = load_input()
    total_count = 0
    for line in values:
        total_count += find_segment_count(line, 2)[0]
        total_count += find_segment_count(line, 4)[0]
        total_count += find_segment_count(line, 3)[0]
        total_count += find_segment_count(line, 7)[0]
    return total_count


def crunch(line):
    """
    This is ugly, but it works.

    Numbers:
    0 - 6 segments, is not 9, is not 6
    1 - is given
    2 - 5 segments, is not 3, is not 5
    3 - 5 segments, does not contain E and is not 5
    4 - is given
    5 - 5 segments, does not contain segment C
    6 - 6 segments, is not 9, has one segment from 1
    7 - is given
    8 - is given
    9 - 6 segments + contains all segments of 4

    Segments:
    A - is the extra value when comparing 1 and 7
    B -
    C - is the missing segment from 6
    D -
    E - is the missing segment from 9
    F -
    G -

    """
    # print(line)

    segment_mapping = {}
    number_mapping = {
        1: get_segment_matcher(line, 2)[0],
        4: get_segment_matcher(line, 4)[0],
        7: get_segment_matcher(line, 3)[0],
        8: get_segment_matcher(line, 7)[0]
    }

    nine = [x for x in get_segment_matcher(line, 6) if len(set(x) & set(number_mapping[4])) == 4]
    number_mapping[9] = nine[0]

    six_candidates = [x for x in get_segment_matcher(line, 6) if x != number_mapping[9]]
    six_candidates = [x for x in six_candidates if len(set(x) & set(number_mapping[1])) == 1]
    number_mapping[6] = six_candidates[0]

    zero_candidates = [x for x in get_segment_matcher(line, 6) if x != number_mapping[9] and x != number_mapping[6]]
    number_mapping[0] = zero_candidates[0]

    segment_e = list(set('abcdefg') ^ set(number_mapping[9]))[0]
    segment_mapping['e'] = segment_e

    segment_c = list(set('abcdefg') ^ set(number_mapping[6]))[0]
    segment_mapping['c'] = segment_c

    five = [x for x in get_segment_matcher(line, 5) if segment_mapping['c'] not in x][0]
    number_mapping[5] = five

    three = [x for x in get_segment_matcher(line, 5) if segment_mapping['e'] not in x and x != number_mapping[5]][0]
    number_mapping[3] = three

    two = [x for x in get_segment_matcher(line, 5) if x != number_mapping[3] and x != number_mapping[5]][0]
    number_mapping[2] = two

    return number_mapping


def get_matching_number(raw_num, mapping):
    for key, val in mapping.items():
        if val == raw_num:
            return key
    return 'NaN'


def part2():
    values = load_input()
    summed = 0
    for val in values:
        computed = crunch(val)
        numbers_list = [''.join(sorted(x)) for x in val[1].split(' ')]
        actual_number_string = ''
        for num in numbers_list:
            actual_number_string += str(get_matching_number(num, computed))
        summed += int(actual_number_string)
    return summed


part1_solution = part1()
print(f'The solution for part 1 is {part1_solution}')
part2_solution = part2()
print(f'The solution for part 2 is {part2_solution}')
