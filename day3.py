in_value = open('input-files/day3-input.txt').readlines()


def get_most_common_bit_at_index(value_list, index):
    val = 0
    for value in value_list:
        if value[index] == '1':
            val += 1
        else:
            val -= 1

    if val > 0:
        return 1
    elif val < 0:
        return 0
    else:
        return -1


def part1():
    gamma_list = [str(get_most_common_bit_at_index(in_value, x)) for x in range(len(in_value[0]) - 1)]

    gamma_string = ''.join(gamma_list)

    gamma = int(gamma_string, 2)
    epsilon = 0b111111111111 - gamma

    return gamma * epsilon


def part2():
    valid_oxy = [x.strip() for x in in_value]
    for i in range(12):
        target = str(get_most_common_bit_at_index(valid_oxy, i))
        target = '1' if target == '-1' else target

        valid_oxy = [x for x in valid_oxy if x[i] == target]
        if len(valid_oxy) == 1:
            break

    valid_co2 = [x.strip() for x in in_value]
    for i in range(12):
        target = '1' if get_most_common_bit_at_index(valid_co2, i) == 0 else '0'
        target = '0' if target == '-1' else target

        valid_co2 = [x for x in valid_co2 if x[i] == target]
        if len(valid_co2) == 1:
            break

    return int(valid_oxy[0], 2) * int(valid_co2[0], 2)


print(part1())
print(part2())
