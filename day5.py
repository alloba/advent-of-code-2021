def get_coordinate_pairs(text_file_in):
    read_lines = open("input-files/day5-input.txt", "r").readlines()
    pairs = []
    for index, line in enumerate(read_lines):
        coord1 = line.split(' -> ')[0]
        coord2 = line.split(' -> ')[1]
        pairs.append([])
        pairs[index].append([int(x.strip()) for x in (coord1.split(','))])
        pairs[index].append([int(x.strip()) for x in (coord2.split(','))])

    return pairs


def get_slope(coord_pair):
    rise = coord_pair[1][1] - coord_pair[0][1]
    run = coord_pair[1][0] - coord_pair[0][0]

    if run == 0:
        return 'inf'
    else:
        slope = rise / run
        return int(slope)  # problem space always has a whole number slope


def mutate_coordinates(coordinates_dict, coord_pairs):
    for coord_pair in coord_pairs:
        slope = get_slope(coord_pair)

        if slope == 'inf':
            maxy = max(coord_pair[0][1], coord_pair[1][1])
            miny = min(coord_pair[0][1], coord_pair[1][1])
            # print(coord_pair)
            for y in range(miny, maxy + 1, 1):
                if f'{coord_pair[0][0]},{y}' in coordinates_dict:
                    coordinates_dict[f'{coord_pair[0][0]},{y}'] += 1
                else:
                    coordinates_dict[f'{coord_pair[0][0]},{y}'] = 1

        elif slope == 1 or slope == -1:
            stepx = -1 if coord_pair[0][0] >= coord_pair[1][0] else 1
            stepy = -1 if coord_pair[0][1] >= coord_pair[1][1] else 1

            xrange = range(coord_pair[0][0], coord_pair[1][0] + stepx, stepx)
            yrange = range(coord_pair[0][1], coord_pair[1][1] + stepy, stepy)

            for x,y in zip(list(xrange), list(yrange)):
                if f'{x},{y}' in coordinates_dict:
                    coordinates_dict[f'{x},{y}'] += 1
                else:
                    coordinates_dict[f'{x},{y}'] = 1
        else:
            step = -1 if coord_pair[0][0] >= coord_pair[1][0] else 1
            for itera, x in enumerate(range(coord_pair[0][0], coord_pair[1][0] + step, step)):
                y = coord_pair[0][1] + (slope * itera)
                if f'{x},{y}' in coordinates_dict:
                    coordinates_dict[f'{x},{y}'] += 1
                else:
                    coordinates_dict[f'{x},{y}'] = 1


def part1():
    coordinates_dict = {}
    coord_pairs = get_coordinate_pairs("input-files/day5-input.txt")
    filtered_coords = [x for x in coord_pairs if get_slope(x) == 'inf' or get_slope(x) == 0]
    mutate_coordinates(coordinates_dict, filtered_coords)

    print(len([x for x in coordinates_dict.values() if x > 1]))


def part2():
    coordinates_dict = {}
    coord_pairs = get_coordinate_pairs("input-files/day5-input.txt")
    mutate_coordinates(coordinates_dict, coord_pairs)

    print(len([x for x in coordinates_dict.values() if x > 1]))


part1()
part2()

